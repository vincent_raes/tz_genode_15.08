/*
 * \brief  Driver for the L2 pl310 Cache Controller
 * \author Vincent Raes
 * \date   
 */

/*
 * Copyright (C) 2012-2016 Genode Labs GmbH
 *
 * This file is part of the Genode OS framework, which is distributed
 * under the terms of the GNU General Public License version 2.
 */

#ifndef _BASE_HW__SRC__SERVER__VMM__PL_310_H_
#define _BASE_HW__SRC__SERVER__VMM__PL_310_H_

/* Genode includes */
#include <util/mmio.h>

class Pl_310 : Genode::Mmio
{
	private:

		/* Used in switch to determine what register to change */
		enum {
			CONTROL 	= 0x100,
			AUXCTRL 	= 0x104,
			TAGRAMCTRL	= 0x108,
			DATARAMCTRL	= 0x10C,
			ADDRFILTERSTART = 0xC00,
			ADDRFILTEREND 	= 0xC04,
			DEBUGCTRL 	= 0xF40,
			PREFETCHCTRL 	= 0xF60,
			POWERCTRL 	= 0xF80
		};

		struct Control : public Register<CONTROL, 32>
		{
			struct Cache_enable 		: Bitfield<0,1> { };
		};

		struct Aux_control : public Register<AUXCTRL, 32>
		{
			struct Full_line_of_zero_en   	: Bitfield<0,1>  { };
			struct High_prio_SO_dev_en     	: Bitfield<10,1> { };
			struct Store_buf_dev_limit_en  	: Bitfield<11,1> { };
			struct Excl_cache_config       	: Bitfield<12,1> { };
			struct Shared_attr_inval_en    	: Bitfield<13,1> { };
			struct Associativity	       	: Bitfield<16,1> { };
			struct Way_size		       	: Bitfield<17,3> { };
			struct Event_monitor_bus_en    	: Bitfield<20,1> { };
			struct Parity_en	       	: Bitfield<21,1> { };
			struct Shared_attr_override_en 	: Bitfield<22,1> { };
			struct Force_write_allocate	: Bitfield<23,2> { };
			struct Cache_replac_policy	: Bitfield<25,1> { };
			struct NS_lockdown_en		: Bitfield<26,1> { };
			struct NS_int_access_ctrl	: Bitfield<27,1> { };
			struct Data_prefetch_en		: Bitfield<28,1> { };
			struct Instr_prefetch_en	: Bitfield<29,1> { };
			struct Early_BRESP_en		: Bitfield<30,1> { };
		};

		struct Tag_RAM_latency_ctrl : public Register<DATARAMCTRL, 32>
		{
			struct RAM_setup_latency	: Bitfield<0,3> { };
			struct RAM_read_access_latency	: Bitfield<4,3> { };
			struct RAM_write_access_latency	: Bitfield<8,3> { };
		};

		struct Data_RAM_latency_ctrl : public Register<TAGRAMCTRL, 32>
		{
			struct RAM_setup_latency	: Bitfield<0,3> { };
			struct RAM_read_access_latency	: Bitfield<4,3> { };
			struct RAM_write_access_latency	: Bitfield<8,3> { };
		};

		struct Address_filter_start : public Register<ADDRFILTERSTART, 32>
		{
			struct Addr_filter_en		: Bitfield<0,1>   { };
			struct Addr_filter_start	: Bitfield<20,12> { };
		};

		struct Address_filter_end : public Register<ADDRFILTEREND, 32>
		{
			struct Addr_filter_end		: Bitfield<20,12> { };
		};

		struct Debug_ctrl : public Register<DEBUGCTRL, 32>
		{
			struct Disable_cache_linefill	: Bitfield<0,1> { };
			struct Disable_write_back	: Bitfield<1,1> { };
			struct SNIDEN			: Bitfield<2,1> { };
		};

		struct Prefetch_ctrl : public Register<0xF60, 32>
		{
			struct Prefetch_off  	     	: Bitfield<0,5>  { };
			struct Not_same_ID   	     	: Bitfield<21,1>  { };
			struct Incr_double_lf_en     	: Bitfield<23,1>  { };
			struct Prefetch_drop_en      	: Bitfield<24,1>  { };
			struct Double_lf_on_WRAP_dis 	: Bitfield<27,1>  { };
			struct Data_prefetch_en      	: Bitfield<28,1>  { };
			struct Instr_prefetch_en     	: Bitfield<29,1>  { };
			struct Double_lf_en          	: Bitfield<23,1>  { };
		};

		struct Power_ctrl : public Register<0xF80, 32>
		{
			struct Standby_mode_en       	: Bitfield<0,1> {};
			struct Dynamic_clk_gating_en 	: Bitfield<1,1> {};
		};


	public:

		Pl_310(Genode::addr_t const base) : Genode::Mmio(base) {
			_write_verbose=1;
			_read_verbose=1;

			read<Control>();
			write<Aux_control::NS_lockdown_en> (1);
		}

		bool ctrl(Genode::addr_t offset, Genode::addr_t val)
		{

			bool need = true;

			switch(offset) {
			case CONTROL:
				Genode::printf("Writing %08x to Control register\n", (int)val);
				write<Control> (val);
				break;
			case AUXCTRL:
				Genode::printf("Writing %08x to Auxilary Control register\n", (int)val);
				write<Aux_control> (val);
				break;
			case TAGRAMCTRL:
				Genode::printf("Writing %08x to Tag RAM latency Control register\n", (int)val);
				write<Tag_RAM_latency_ctrl> (val);
				break;
			case DATARAMCTRL:
				Genode::printf("Writing %08x to Data RAM latency Control register\n", (int)val);
				write<Data_RAM_latency_ctrl> (val);
				break;
			case ADDRFILTERSTART:
				Genode::printf("Writing %08x to Address filtering start register\n", (int)val);
				write<Address_filter_start> (val);
				break;
			case ADDRFILTEREND:
				Genode::printf("Writing %08x to Address filtering end register\n", (int)val);
				write<Address_filter_end> (val);
				break;
			case DEBUGCTRL:
				Genode::printf("Writing %08x to Debug Control register\n", (int)val);
				write<Debug_ctrl> (val);
				break;
			case PREFETCHCTRL:
				Genode::printf("Writing %08x to Prefetch Control register\n", (int)val);
				write<Prefetch_ctrl> (val);
				break;
			case POWERCTRL:
				Genode::printf("Writing %08x to Power Control register\n", (int)val);
				write<Power_ctrl> (val);
				break;
			default:
				Genode::printf("Not necessary to use Secure World for write\n");
				need = false;
			}

			return need;
		}
};

#endif /* _BASE_HW__SRC__SERVER__VMM__TSC_380_H_ */
