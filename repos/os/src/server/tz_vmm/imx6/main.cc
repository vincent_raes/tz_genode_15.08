/*
 * \brief  Virtual Machine Monitor
 * \author Stefan Kalkowski
 * \date   2012-06-25
 */

/*
 * Copyright (C) 2008-2012 Genode Labs GmbH
 *
 * This file is part of the Genode OS framework, which is distributed
 * under the terms of the GNU General Public License version 2.
 */

/* Genode includes */
#include <base/env.h>
#include <base/sleep.h>
#include <base/thread.h>
#include <drivers/board_base.h>
#include <drivers/trustzone.h>
#include <vm_state.h>

/* local includes */
#include <tsc_380.h>
#include <vm.h>
//#include <block.h>


using namespace Genode;

namespace Vmm {
	class Vmm;
}

typedef uint32_t Timestamp;

Timestamp timestamp()
{
	uint32_t t;
	asm volatile("mrc p15, 0, %0, c9, c13, 0" : "=r"(t));
	return t;
}

class Vmm::Vmm : public Thread<8192>
{
	private:

		enum Hypervisor_calls {
			CACHE_CTRL = 1,
			CCM        = 2,
			BLOCK      = 3
		};


		Signal_receiver           _sig_rcv;
		Signal_context            _vm_context;

		Io_mem_connection         _tsc_io_mem;

		Tsc_380                   _tsc;

		Vm                       *_vm;
		//Block                     _block;
		int                        teller;
		Timestamp                  time;


		void _handle_hypervisor_call()
		{
			switch (_vm->smc_arg_0()) {
			case BLOCK:
				//_block.handle(_vm);
				break;
			default:
				teller++;
				/*unsigned int v = 0;
				asm volatile("mrs %0, cpsr" : "=r" (v) ::);
				Genode::printf("Value of the CPSR register 0x%08x\n", v);
				v = 0;
				asm volatile("mrs %0, spsr" : "=r" (v) ::);
				Genode::printf("Value of the SPSR register 0x%08x\n", v);
				v = 0;
				asm volatile("mrs %0, apsr" : "=r" (v) ::);
				Genode::printf("Value of the APSR register 0x%08x\n", v);*/
				/*Genode::printf("debug nr: %d (0x%08x)\n", (int)_vm->state()->r10, (unsigned int)_vm->state()->r10);
				Genode::printf("Overarching counter: %d (0x%08x)\n", (int)_vm->state()->r12, (unsigned int)_vm->state()->r12);*/
				_vm->state()->r0 = teller;
				_vm->state()->r4 = teller;
				_vm->state()->r5 = teller;
				
				//PERR("Unknown hypervisor call!");
				//_vm->dump();
			}
		}

		bool _handle_data_abort()
		{
			PWRN("Vm tried to access %p which isn't allowed",
			     _tsc.last_failed_access());
			_vm->dump();
			return false;
		}

		bool _handle_vm()
		{
			//PINF("HANDLING VM");
			switch (_vm->state()->cpu_exception) {
			case Cpu_state::DATA_ABORT:
				if (!_handle_data_abort()) {
					PERR("Could not handle data-abort will exit!");
					return false;
				}
				break;
			case Cpu_state::SUPERVISOR_CALL:
				_handle_hypervisor_call();
				break;
			default:
				PERR("Curious exception occured");
				_vm->dump();
				return false;
			}
			return true;
		}

	protected:

		void entry()
		{
			_vm->sig_handler(_sig_rcv.manage(&_vm_context));
			_vm->start();

			while (true) {
				//PINF("TO NORMAL WORLD");
				time = timestamp();
				_vm->state()->r4 = time;
				_vm->run();
				Signal s = _sig_rcv.wait_for_signal();
				//PDBG("Signal recieved");
				if (s.context() != &_vm_context) {
					PWRN("Invalid context");
					continue;
				}
				if (!_handle_vm())
					return;
			}
		};
		

	public:

		Vmm(addr_t tsc_base, 
			Vm    *vm)
		: Thread<8192>("vmm"),
		  _tsc_io_mem(tsc_base,     0x4000),
		  _tsc((addr_t)env()->rm_session()->attach(_tsc_io_mem.dataspace())),
		  _vm(vm) { 
			teller = 0;
		}
};


int main()
{
	enum {
		TSC_IMX6_BASE     = 0x021D0000, // Length = 0x4000
		MAIN_MEM_START    = Trustzone::NONSECURE_RAM_BASE,
		MAIN_MEM_SIZE     = Trustzone::NONSECURE_RAM_SIZE,
		KERNEL_OFFSET     = 0x8000,
		MACH_TYPE         = 4296, // voor nitrogen6x
				/*3769, voor SABRElite source: www.arm.linux.org.uk/developer/machines*/
	};

	static const char* cmdline = "console=ttymxc1,115200 init=/init video=mxcfb0:dev=hdmi,1920x1080M@60,if=RGB24 fbmem=10M vmalloc=400M androidboot.console=ttymxc1 loglevel=10 earlyprintk";

	static Vm vm("android", /*"initrd.gz",*/ cmdline, MAIN_MEM_START, MAIN_MEM_SIZE,
	             KERNEL_OFFSET, MACH_TYPE);

	static Vmm::Vmm vmm(TSC_IMX6_BASE, 
	                    &vm);

	PINF("Start virtual machine ...");
	vmm.start();

	sleep_forever();
	return 0;
}
