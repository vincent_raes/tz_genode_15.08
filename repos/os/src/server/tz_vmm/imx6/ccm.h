/*
 * \brief  Driver for the L2 pl310 Cache Controller
 * \author Vincent Raes
 * \date   
 */

/*
 * Copyright (C) 2012-2016 Genode Labs GmbH
 *
 * This file is part of the Genode OS framework, which is distributed
 * under the terms of the GNU General Public License version 2.
 */

#ifndef _BASE_HW__SRC__SERVER__VMM__CCM_H_
#define _BASE_HW__SRC__SERVER__VMM__CCM_H_

/* Genode includes */
#include <util/mmio.h>

class Ccm : Genode::Mmio
{
	private:

		enum {
			CONTROL			= 0x000,
			CONTROL_DIVIDER 	= 0x004,
			CLOCK_SWITCHER  	= 0x00C
		};

		struct Control : public Register<CONTROL, 32>
		{
			struct Oscnt	 		: Bitfield<0,8>  { };
			struct Cosc_en			: Bitfield<12,1> { };
			struct Wb_count			: Bitfield<16,3> { };
			struct Reg_bypass_count		: Bitfield<21,6> { };
			struct Rbc_en			: Bitfield<27,1> { };
		};

		struct Control_divider : public Register<CONTROL_DIVIDER, 32>
		{
			struct mmdc_ch1_mask 		: Bitfield<16,1> { };
			struct mmdc_ch0_mask		: Bitfield<17,1> { };
		};

		struct Clock_switcher : public Register<CLOCK_SWITCHER, 32>
		{
			struct pll3_sw_clk_sel 		: Bitfield<0,1>  { };
			struct pll1_sw_clk_sel		: Bitfield<2,1>  { };
			struct step_sel			: Bitfield<8,1>  { };
			struct pll2_pfd2_dis_mask	: Bitfield<9,1>  { };
			struct pll2_pfd0_dis_mask	: Bitfield<10,1> { };
			struct pll2_pfd1_594m_dis_mask	: Bitfield<11,1> { };
			struct pll3_pfd2_dis_mask	: Bitfield<12,1> { };
			struct pll3_pfd3_dis_mask	: Bitfield<13,1> { };
			struct pll3_pfd0_dis_mask	: Bitfield<14,1> { };
			struct pll3_pfd1_dis_mask	: Bitfield<15,1> { };
		};


	public:

		Ccm(Genode::addr_t const base) : Genode::Mmio(base) {
			_write_verbose=1;
			_read_verbose=1;

			read<Clock_switcher>();
		}

		void print_switcher(){
			read<Clock_switcher>();
		}

		
};

#endif /* _BASE_HW__SRC__SERVER__VMM__TSC_380_H_ */
