#
# \brief  Build config for parts of core that depend on Trustzone status
# \author Stefan Kalkowski
# \author Martin Stein
# \date   2012-10-24
#

# add C++ sources
SRC_CC += kernel/vm_thread.cc
SRC_CC += spec/imx6/platform_support.cc
SRC_CC += spec/arm_gic/pic.cc
SRC_CC += platform_services.cc

# include less specific configuration
include $(REP_DIR)/lib/mk/platform_imx6/core-trustzone.inc
include $(REP_DIR)/lib/mk/core-trustzone.inc
