#
# \brief  Build config for Genodes core process
# \author Stefan Kalkowski
# \author Martin Stein
# \date   2012-10-24
#

# add include paths
INC_DIR += $(REP_DIR)/src/core/include/spec/imx6
INC_DIR += $(REP_DIR)/src/core/include/spec/imx

# include less specific configuration
include $(REP_DIR)/lib/mk/cortex_a9/core.inc
