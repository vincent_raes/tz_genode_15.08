TARGET    = tz_vmm
REQUIRES  = trustzone platform_imx6
LIBS      = base libc libcrypto libssl
SRC_CC    = main.cc
INC_DIR  += $(PRG_DIR) \
            $(PRG_DIR)/../include \
            $(PRG_DIR)/../imx6

vpath main.cc $(PRG_DIR)/../imx6
