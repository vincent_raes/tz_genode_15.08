/*
 * \brief  Crypto functions (symmetric and assymmetric)
 * \author Vincent Raes
 * \date   2016-02-24
 */

/*
 * Copyright (C) 2013 Genode Labs GmbH
 *
 * This file is part of the Genode OS framework, which is distributed
 * under the terms of the GNU General Public License version 2.
 */

#ifndef _SRC__SERVER__VMM__INCLUDE__CRYPTO_H_
#define _SRC__SERVER__VMM__INCLUDE__CRYPTO_H_

/* Genode includes */
#include <base/printf.h>
#include <base/env.h>
#include <dataspace/client.h>

/* Openssl includes */
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/x509.h>
#include <openssl/x509_vfy.h>
#include <openssl/rand.h>
#include <openssl/conf.h>

/* Local includes */
#include <vm.h>
#include <util.h>

class Crypto{

	private:

		unsigned char 		 encrypted[4096] = {};
		unsigned char 		 decrypted[4096] = {};
		unsigned char 		 temp[4096] = {};
		unsigned char 		*data;
		unsigned char 		*publicKey;
		unsigned char 		*privateKey;
		unsigned char 		*symmKey;
		int written;
		int padding;
		Ram	      		*_ram;
		Genode::Vm_state	*_state;

		RSA * createRSA(unsigned char * key, int publiek)
		{
			RSA *rsa= NULL;
			BIO *keybio ;
			keybio = BIO_new_mem_buf(key, -1);
			if (keybio==NULL)
			{
				Genode::printf( "Failed to create key BIO\n");
				return 0;
			}
			if(publiek)
			{
				rsa = PEM_read_bio_RSAPublicKey(keybio, &rsa, NULL, NULL);
			}
			else
			{
				rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL);
			}
			if(rsa == NULL)
			{
				Genode::printf( "Failed to create RSA\n");
			}
		 
			return rsa;
		}
		RSA * createRSAFile(unsigned char * URI, int publiek)
		{
			RSA *rsa= NULL;
			BIO *keybio ;
			keybio = BIO_new(BIO_s_file());
			BIO_read_filename(keybio, URI);
			if (keybio==NULL)
			{
				Genode::printf( "Failed to create key BIO\n");
				return 0;
			}
			if(publiek)
			{
				rsa = PEM_read_bio_RSAPublicKey(keybio, &rsa, NULL, NULL);
			}
			else
			{
				rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL);
			}
			if(rsa == NULL)
			{
				Genode::printf( "Failed to create RSA\n");
			}
		 
			return rsa;
		}

		int public_encrypt(unsigned char * data,int data_len,unsigned char * key, unsigned char *encrypted)
		{
			RSA * rsa = createRSA(key,1);
			Genode::printf("public key aangemaakt\n");
			Genode::printf("key size = %u\n", RSA_size(rsa));
			int result = RSA_public_encrypt(data_len,data,encrypted,rsa,padding);
			return result;
		}
		int private_decrypt(unsigned char * enc_data,int data_len,unsigned char * key, unsigned char *decrypted)
		{
			RSA * rsa = createRSA(key,0);
			Genode::printf("private key aangemaakt\n");
			Genode::printf("key size = %u\n", RSA_size(rsa));
			int  result = RSA_private_decrypt(data_len,enc_data,decrypted,rsa,padding);
			return result;
		}

		int encryptSymm(unsigned char *plaintext, int plaintext_len, unsigned char *key,
		  unsigned char *iv, unsigned char *ciphertext)
		{
		  	EVP_CIPHER_CTX *ctx;

		  	int len;

		  	int ciphertext_len;

		  	if(!(ctx = EVP_CIPHER_CTX_new())) 
		  		Genode::printf("Error initializing context\n");

			if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv))
		  		Genode::printf("Error initializing encrypt\n");

		  	if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
		  		Genode::printf("Error providing message\n");
			ciphertext_len = len;

		  	if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) 
			  	Genode::printf("Error encrypting\n");
		  	ciphertext_len += len;

		  	EVP_CIPHER_CTX_free(ctx);

		  	return ciphertext_len;
		}

		int authEncrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
		  unsigned char *iv, unsigned char *ciphertext, unsigned char *tag)
		{
		  	EVP_CIPHER_CTX *ctx;

		  	int len;

		  	int ciphertext_len;
			const unsigned char *aad = NULL;
			Util::getAAD(aad);
			int aad_len = sizeof(aad);

		  	if(!(ctx = EVP_CIPHER_CTX_new())) 
		  		Genode::printf("Error initializing context\n");

		  	if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_gcm(), NULL, key, iv))
		  		Genode::printf("Error initializing encrypt\n");

		  	if(1 != EVP_EncryptUpdate(ctx, NULL, &len, aad, aad_len))
			  	Genode::printf("Error initializing aad\n"); 

			if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
			  	Genode::printf("Error providing message\n");
			ciphertext_len = len;

			if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) 
			  	Genode::printf("Error encrypting\n");
			ciphertext_len += len;

			if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, tag))
			  	Genode::printf("Error getting tag\n");

			EVP_CIPHER_CTX_free(ctx);

			return ciphertext_len;
		}
		int authDecrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
		  unsigned char *iv, unsigned char *plaintext)
		{
			EVP_CIPHER_CTX *ctx;

			int len;

			int plaintext_len;
			const unsigned char *aad = NULL;
			Util::getAAD(aad);
			int aad_len = sizeof(aad);
			int ret;

			char *tag = NULL;
			Util::split((char*)ciphertext, ciphertext_len - Util::getTagSize(), (char*)encrypted, tag);

			if(!(ctx = EVP_CIPHER_CTX_new())) 
		  		Genode::printf("Error initializing context\n");

			if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_gcm(), NULL, key, iv))
			  	Genode::printf("Error initializing decrypt\n");

			if(1 != EVP_DecryptUpdate(ctx, NULL, &len, aad, aad_len))
			  	Genode::printf("Error initializing aad\n");

			if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, encrypted, ciphertext_len - Util::getTagSize()))
			  	Genode::printf("Error providing message\n");
			plaintext_len = len;

			if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 16, tag))
			  	Genode::printf("Error providing expected tag\n");

			ret = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);
			plaintext_len += len;

			EVP_CIPHER_CTX_free(ctx);

			if(ret > 0){
				plaintext_len += len;
				return plaintext_len;
			} else {
				return -1;
			}
		}

		int sign(unsigned char * data, int data_len, unsigned char * key, unsigned char **outSig, unsigned int *outSig_length)
		{
			RSA * rsa = createRSAFile(key,0);
			Genode::printf("private key aangemaakt\n");
			Genode::printf("key size = %u\n", RSA_size(rsa));
	
			unsigned char *sig = NULL;
			unsigned int sig_len = 0;

			// Attempt to hash the data
			int rc = 1;
			SHA_CTX sha_ctx = { 0 };
			unsigned char digest[SHA_DIGEST_LENGTH];
			rc = SHA1_Init(&sha_ctx);
			if (1 != rc) {Genode::printf("SHA_Init failed");}
			rc = SHA1_Update(&sha_ctx, data, data_len);
			if (1 != rc) {Genode::printf("SHA_Update failed");}
			rc = SHA1_Final(digest, &sha_ctx);
			if (1 != rc) {Genode::printf("SHA_Final failed");}

			sig = (unsigned char*)malloc(RSA_size(rsa));

			rc = RSA_sign(NID_sha1, digest, sizeof digest, sig, &sig_len, rsa);

			*outSig = sig;
			*outSig_length = sig_len;
			return rc;
		}
		int verify(unsigned char * data, int data_len, unsigned char * key, unsigned char *sig, unsigned int sig_length)
		{
			RSA * rsa = createRSA(key,1);
			Genode::printf("public key aangemaakt\n");
			Genode::printf("key size = %u\n", RSA_size(rsa));

			// Attempt to hash the data
			int rc = 1;
			SHA_CTX sha_ctx = { 0 };
			unsigned char digest[SHA_DIGEST_LENGTH];
			rc = SHA1_Init(&sha_ctx);
			if (1 != rc) {Genode::printf("SHA_Init failed");}
			rc = SHA1_Update(&sha_ctx, data, data_len);
			if (1 != rc) {Genode::printf("SHA_Update failed");}
			rc = SHA1_Final(digest, &sha_ctx);
			if (1 != rc) {Genode::printf("SHA_Final failed");}

			rc = RSA_verify(NID_sha1, digest, sizeof digest, sig, sig_length, rsa);
			return rc;
		}
		int verifySig(unsigned char * data, int data_len, unsigned char * certArray, unsigned char *sig, unsigned int sig_length)
		{
			BIO *certbio = NULL;
			EVP_PKEY * pubkey = NULL;
			RSA * rsa = NULL;
			X509 *cert = NULL;

			certbio = BIO_new_mem_buf(certArray, -1);
			cert = PEM_read_bio_X509(certbio, NULL, 0, NULL);

			pubkey = X509_get_pubkey (cert);
			rsa = EVP_PKEY_get1_RSA(pubkey);
			Genode::printf("public key aangemaakt\n");
			Genode::printf("key size = %u\n", RSA_size(rsa));

			// Attempt to hash the data
			int rc = 1;
			SHA_CTX sha_ctx = { 0 };
			unsigned char digest[SHA_DIGEST_LENGTH];
			rc = SHA1_Init(&sha_ctx);
			if (1 != rc) {Genode::printf("SHA_Init failed");}
			rc = SHA1_Update(&sha_ctx, data, data_len);
			if (1 != rc) {Genode::printf("SHA_Update failed");}
			rc = SHA1_Final(digest, &sha_ctx);
			if (1 != rc) {Genode::printf("SHA_Final failed");}

			rc = RSA_verify(NID_sha1, digest, sizeof digest, sig, sig_length, rsa);
			return rc;
		}
		int verifySigFile(unsigned char * data, int data_len, const char * certPath, unsigned char *sig, unsigned int sig_length)
		{
			BIO *certbio = NULL;
			EVP_PKEY * pubkey = NULL;
			RSA * rsa = NULL;
			X509 *cert;

			certbio = BIO_new(BIO_s_file());
			BIO_read_filename(certbio, certPath);
			cert = PEM_read_bio_X509(certbio, NULL, 0, NULL);

			pubkey = X509_get_pubkey (cert);
			rsa = EVP_PKEY_get1_RSA(pubkey);
			Genode::printf("public key aangemaakt\n");
			Genode::printf("key size = %u\n", RSA_size(rsa));

			// Attempt to hash the data
			int rc = 1;
			SHA_CTX sha_ctx = { 0 };
			unsigned char digest[SHA_DIGEST_LENGTH];
			rc = SHA1_Init(&sha_ctx);
			if (1 != rc) {Genode::printf("SHA_Init failed");}
			rc = SHA1_Update(&sha_ctx, data, data_len);
			if (1 != rc) {Genode::printf("SHA_Update failed");}
			rc = SHA1_Final(digest, &sha_ctx);
			if (1 != rc) {Genode::printf("SHA_Final failed");}

			rc = RSA_verify(NID_sha1, digest, sizeof digest, sig, sig_length, rsa);
			return rc;
		}

		int verifyCert(unsigned char *certArray, const char *caURI){
			BIO              *certbio = NULL;
			BIO               *outbio = NULL;
			X509                *cert = NULL;
			X509_STORE         *store = NULL;
			X509_STORE_CTX  *vrfy_ctx = NULL;
			int ret;

			certbio = BIO_new_mem_buf(certArray, -1);
			outbio  = BIO_new_fp(stdout, BIO_NOCLOSE);

			if (!(store=X509_STORE_new()))
				BIO_printf(outbio, "Error creating X509_STORE_CTX object\n");

			vrfy_ctx = X509_STORE_CTX_new();

			if (! (cert = PEM_read_bio_X509(certbio, NULL, 0, NULL))) {
				BIO_printf(outbio, "Error loading cert into memory\n");
			}

			ret = X509_STORE_load_locations(store, caURI, NULL);
			if (ret != 1)
				BIO_printf(outbio, "Error loading CA cert or chain file\n");

			X509_STORE_CTX_init(vrfy_ctx, store, cert, NULL);

			ret = X509_verify_cert(vrfy_ctx);
			BIO_printf(outbio, "Verification return code: %d\n", ret);

			if(ret == 0 || ret == 1)
			BIO_printf(outbio, "Verification result text: %s\n",
				X509_verify_cert_error_string(vrfy_ctx->error));

			X509_STORE_CTX_free(vrfy_ctx);
			X509_STORE_free(store);
			X509_free(cert);
			BIO_free_all(certbio);
			BIO_free_all(outbio);
		
			return ret;
		}

	public:
		Crypto(Vm *vm):
		_ram(vm->ram()), _state(vm->state()){
			padding = RSA_PKCS1_PADDING;
			OpenSSL_add_all_algorithms();
			ERR_load_BIO_strings();
			ERR_load_crypto_strings();
		}

		void authEncrypt(char *input, int size, char *output){
			char *iv = NULL;
			char *tag = NULL;

			generateNonce(Util::getIVSize(), 0, iv);
			int encryptedLength = authEncrypt((unsigned char*)input, size, symmKey, (unsigned char*)iv, encrypted, (unsigned char*)tag);
			for(int i = 0; i < encryptedLength; i++)
				output[i] = encrypted[i];
			for(int i = 0; i < (int)sizeof(tag); i++)
				output[i + encryptedLength] = tag[i];
		}

		void authDecrypt(Genode::addr_t addr, Genode::addr_t size, char *output){
			data = (unsigned char*)(addr);
			char *iv = NULL;

			Util::split((char*)data, Util::getIVSize(), iv, (char*)encrypted);
			int decryptedLength = authDecrypt(encrypted, size - Util::getIVSize(), symmKey, (unsigned char*)iv, decrypted);
			for(int i = 0; i < decryptedLength; i++)
				output[i] = decrypted[i];
		}

		void verifyHPSig(Genode::addr_t addr, Genode::addr_t size, int keyLen, char *output){
			data = (unsigned char*)(addr);
			char *path = NULL;
			for(int i = 0; i < keyLen; i++)
				temp[i + Util::getNonceSize()] = symmKey[i];

			Util::getHpCertPath(path);
			verifySigFile(temp, Util::getNonceSize() + keyLen, path, data, size);
			char ack[] = "ACK";
			char *iv = NULL;
			generateNonce(Util::getIVSize(), 0, iv);
			int encryptedLength = encryptSymm((unsigned char*)ack, sizeof(ack), symmKey, (unsigned char*)iv, encrypted);
			if(encryptedLength == 0)
				Genode::printf("Symmetric encryption failed");

			for(int i = 0; i < encryptedLength; i++)
				output[i] = encrypted[i];
		}

		int decryptSymmKey(Genode::addr_t addr, Genode::addr_t size){
			data = (unsigned char*)(addr);
			int encLength = size - Util::getSigSize();
			for(int i = 0; i < encLength; i++)
				encrypted[i] = data[i + Util::getSigSize()];

			int decryptedLength = private_decrypt(encrypted, encLength, privateKey, decrypted);
			if(decryptedLength == 0)
				Genode::printf("Decryption of symmetric key failed");
			
			enum{ DS_SIZE = 0x0001000 };
			Genode::Ram_dataspace_capability ds = Genode::env()->ram_session()->alloc(DS_SIZE, Genode::UNCACHED);
			char *attach = Genode::env()->rm_session()->attach(ds);

			Genode::strncpy((char*) attach,(const char*)decrypted, ~0);
			symmKey = (unsigned char*)attach;
			Genode::printf("Symmetric Key:\n%s\n", symmKey);

			return decryptedLength;
		}

		bool verifyHPCert(Genode::addr_t addr, Genode::addr_t size){
			bool check;
			char *path = NULL;
			data = (unsigned char*)(addr);
			Util::getCaCertPath(path);
			if(!verifyCert(data, path))
				check = false;
			else{
				check = true;
				Util::getHpCertPath(path);
				Util::saveFile((char*)data, path, size);
			}
			return check;
		}

		bool verifyNewCert(Genode::addr_t addr, Genode::addr_t size){
			bool check = true;
			data = (unsigned char*)(addr);
			char *path = NULL;
			Util::getChaCertPath(path);
			if(!verifyCert(data, path))
				check = false;
			else{
				Util::getUserCertPath(path);
				Util::saveFile((char*)data, path, size);
			}
			return check;
		}

		bool verifyCombo(Genode::addr_t addr, Genode::addr_t size){
			bool check = true;
			data = (unsigned char*)(addr);
			char *key = NULL;
			char *sig = NULL;
			char *path = NULL;
			Util::split((char*)data, Util::getSigSize(), sig, key);
			Util::getCaCertPath(path);
			if(!verifyCert((unsigned char*)key, path))
				check = false;
			else{
				Util::getChaCertPath(path);
				Util::saveFile(key, path);
			}

			if(!verifySig(temp, Util::getNonceSize(), (unsigned char*)key, (unsigned char*)sig, Util::getSigSize()))
				check = false;

			return check;
		}

		void sign(char *input, char *URI, char *output){
			unsigned int sig_length = 0;
			int sig_result = sign((unsigned char*)input,strlen((const char*)input),(unsigned char*)URI,(unsigned char**)&output,&sig_length);
			if(sig_result != 1)
			{
				Genode::printf((char*)"Signature not succesfully created");
			}
			Genode::printf("Signature succesfull\n");
		}

		void genKeyPair(char *output){
			RSA *rsa = NULL;
			BIGNUM *bne = NULL;
			BIO *bp_public = NULL, *bp_private = NULL;
			int ret = 0;

			enum{ DS_SIZE = 0x00010000 };
			Genode::Ram_dataspace_capability ds = Genode::env()->ram_session()->alloc(DS_SIZE, Genode::UNCACHED);
			char *addr = Genode::env()->rm_session()->attach(ds);

			int bits = 2048;
			unsigned long e = RSA_F4;

			bne = BN_new();
			ret = BN_set_word(bne, e);
			if(ret != 1){
				Genode::printf("Error setting bignum\n");
			}

			rsa = RSA_new();
			ret = RSA_generate_key_ex(rsa, bits, bne, NULL);
			if(ret != 1){
				Genode::printf("Error generating keypair\n");
			}

			bp_public = BIO_new(BIO_s_mem());
			ret = PEM_write_bio_RSAPublicKey(bp_public, rsa);
			if(ret != 1){
				Genode::printf("Error writing public key to memory\n");
			}

			int pubLen = BIO_pending(bp_public);
			unsigned char tempPublicKey[pubLen] = {};
			BIO_read(bp_public, tempPublicKey, pubLen);

			Genode::strncpy((char*) addr,(const char*)tempPublicKey, ~0);
			publicKey = (unsigned char*)addr;
			output = (char*) publicKey;
			Genode::printf("Public Key:\n%s\n", publicKey);

			int privLen = BIO_pending(bp_private);
			unsigned char tempPrivateKey[privLen] = {};
			BIO_read(bp_private, tempPrivateKey, privLen);

			Genode::strncpy((char*) addr + pubLen + 4,(const char*)tempPrivateKey, ~0);
			privateKey = (unsigned char*)(addr + pubLen + 4);
			Genode::printf("Private Key:\n%s\n", privateKey);

			BIO_free_all(bp_public);
			BIO_free_all(bp_private);
			RSA_free(rsa);
			BN_free(bne);
		}

		void getSWCert(char *output){
			int ret;
			BIO *certbio = NULL;
			char *path = NULL;

			Util::getSWCertPath(path);

  			ERR_load_BIO_strings();
  			certbio = BIO_new(BIO_s_file());

			ret = BIO_read_filename(certbio, path);
			if (ret == -1) {
				Genode::printf("Error reading cert from file\n");
			}

			int length = 1354;

			ret = BIO_read(certbio, output, length);
			if(ret != length){
				Genode::printf("Error writing cert to char array. (length does not match)\n");
			}
			Genode::printf("Secure World certificate:\n");
			Genode::printf("%s\n", output);
		}

		void generateNonce(int nonceSize, int save, char *output){
			unsigned char nonce[nonceSize];
			int rc = RAND_bytes(nonce, sizeof(nonce));

			if(rc != 1) {
				Genode::printf("RAND_bytes failed\n");
			}

			Genode::printf("Nonce: \n");
			for(int i = 0; i < (int)sizeof(nonce); i++){
				Genode::printf("0x%02x, ", nonce[i]);
				output[i] = (char)nonce[i];
				if(save == 1)
					temp[i] = nonce[i];
			}
			Genode::printf("\n");
		}
};

#endif /* _SRC__SERVER__VMM__INCLUDE__CRYPTO_H_ */
