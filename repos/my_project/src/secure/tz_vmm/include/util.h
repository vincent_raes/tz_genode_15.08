/*
 * \brief  Utility functions + fixed values
 * \author Vincent Raes
 * \date   2016-02-24
 */

/*
 * Copyright (C) 2013 Genode Labs GmbH
 *
 * This file is part of the Genode OS framework, which is distributed
 * under the terms of the GNU General Public License version 2.
 */

#ifndef _SRC__SERVER__VMM__INCLUDE__UTIL_H_
#define _SRC__SERVER__VMM__INCLUDE__UTIL_H_

/* Genode includes */
#include <base/printf.h>

/* Openssl includes */
#include <openssl/bio.h>

class Util{

	private:
		static const int nonceSize 			= 256;
		static const int sigSize 			= 256;
    		static const int ivSize		 		= 12; // in bytes
    		static const int tagSize			= 16; // in bytes
		static constexpr const unsigned char *aad	= (const unsigned char*)"ThisIsAnAAD";
		static constexpr const char *swCert_filestr 	= "./swCert-file.pem";
		static constexpr const char *swKey_filestr 	= "./swKey-file.pem";
		static constexpr const char *caCert_filestr 	= "./caCert-file.pem";
		static constexpr const char *chaCert_filestr 	= "./chaCert-file.pem";
		static constexpr const char *hpCert_filestr 	= "./hpCert-file.pem";
		static constexpr const char *userCert_filestr 	= "./userCert-file.pem";

	public:
		static int getNonceSize(){
			return nonceSize;
		}
		static int getSigSize(){
			return sigSize;
		}
		static int getIVSize(){
			return ivSize;
		}
		static int getTagSize(){
			return tagSize;
		}

		static void getAAD(const unsigned char *output){
			output = aad;
		}

		static void getSWCertPath(char *output){
			output = (char*) swCert_filestr;
		}

		static void getSWKeyPath(char *output){
			output = (char*) swKey_filestr;
		}

		static void getCaCertPath(char *output){
			output = (char*) caCert_filestr;
		}

		static void getUserCertPath(char *output){
			output = (char*) userCert_filestr;
		}

		static void getChaCertPath(char *output){
			output = (char*) chaCert_filestr;
		}

		static void getHpCertPath(char *output){
			output = (char*) hpCert_filestr;
		}

		static void join(char *base, char *addition, char *output){
			for(int i = 0; i < (signed int)sizeof(base); i++)
				output[i] = base[i];
			for(int i = 0; i < (signed int)sizeof(addition); i++)
				output[i + sizeof(base)] = addition[i];
		}

		static void split(char *base, int mark, char *split1, char *split2){
			for(int i = 0; i < mark; i++)
				split1[i] = base[i];
			for(int i = 0; i < (signed int)(sizeof(base) - mark); i++)
				split2[i] = base[i + mark];
		}

		static void saveFile(char *input, const char *URI){
			saveFile(input, URI, sizeof(input));
		}
		static void saveFile(char *input, const char *URI, int size){
			BIO *outbio = NULL;

			outbio = BIO_new_file(URI, "w");
			BIO_write(outbio, input, size);
			BIO_free_all(outbio);
		}

};

#endif /* _SRC__SERVER__VMM__INCLUDE__UTIL_H_ */
