/*
 * \brief  Write to specific physical addresses
 * \author Vincent Raes
 * \date   2015-03-25
 */

/*
 * Copyright (C) 2013 Genode Labs GmbH
 *
 * This file is part of the Genode OS framework, which is distributed
 * under the terms of the GNU General Public License version 2.
 */

#ifndef _SRC__SERVER__VMM__INCLUDE__RAM_WRITER_H
#define _SRC__SERVER__VMM__INCLUDE__RAM_WRITER_H

/* Genode includes */
#include <util/mmio.h>

class Ram_writer : public Genode::Mmio
{
	private:


	public:

		Ram_writer(Genode::addr_t const base) : Genode::Mmio(base) { }

		void write(char* msg){
			using namespace Genode;

			Genode::memset((void*)base, 0, Genode::strlen((const char*)msg)+1);

			Genode::memcpy((void*) base, (void*) msg, Genode::strlen((const char*)msg));
			unsigned char *data2;
			data2 = (unsigned char*)base;
			Genode::printf("address of data = %08x \n", (int)&data2[0]);
		}

		void read(Genode::size_t size, Genode::Dataspace_capability ds){
			using namespace Genode;

			unsigned char *data;
			data = (unsigned char*) base;
			char *addr = env()->rm_session()->attach(ds);
			Genode::printf("Data at %08x = %s\n", (int)data, (char*)data);
			/*for(int i = 0; i < (int)size; i++){
				buffer[i] = data[i];
			}*/
			Genode::memcpy((void*)addr, (void*)base, size);
			env()->rm_session()->detach(addr);
		}
};

#endif /* _SRC__SERVER__VMM__INCLUDE__RAM_WRITER_H*/
