/*
 * \brief  Virtual Machine Monitor
 * \author Stefan Kalkowski
 * \date   2012-06-25
 */

/*
 * Copyright (C) 2008-2012 Genode Labs GmbH
 *
 * This file is part of the Genode OS framework, which is distributed
 * under the terms of the GNU General Public License version 2.
 */

/* Genode includes */
//#include <base/elf.h>
#include <base/env.h>
#include <base/sleep.h>
#include <base/thread.h>
/*#include <cpu/cpu_state.h>
#include <io_mem_session/connection.h>
#include <rom_session/connection.h>
#include <vm_session/connection.h>
#include <dataspace/client.h>*/
#include <drivers/board_base.h>
#include <drivers/trustzone.h>
#include <vm_state.h>
//#include <util/mmio.h>

/* local includes */
#include <vm.h>
#include <util.h>
//#include <crypto.h>
#include <tsc_380.h>


using namespace Genode;

namespace Vmm {
	class Vmm;
}

class Vmm::Vmm : public Thread<8192>
{
	private:

		enum Hypervisor_calls {
			INIT_STEP1 = 1,
			INIT_STEP2,
			INIT_STEP3,
			REQUEST_STEP1,
			REQUEST_STEP2,
			REQUEST_STEP3,
			REQUEST_STEP4
		};


		Signal_receiver           _sig_rcv;
		Signal_context            _vm_context;
		Io_mem_connection         _tsc_io_mem;
		Tsc_380                   _tsc;
		Vm                       *_vm;
		//Crypto			  _crypto;


		void _handle_hypervisor_call()
		{
			switch (_vm->state()->r0) {
			/*case INIT_STEP1:
				{
					char *nonce = NULL;
					char *cert = NULL;
					char *joint = NULL;
					_crypto.generateNonce(Util::getNonceSize(), 1, nonce);
					_crypto.getSWCert(cert);
					Util::join(nonce, cert, joint);
					_vm->ram()->write(_vm->va_to_pa(_vm->state()->r12) - _vm->ram()->base(), joint);
				}
				break;
			case INIT_STEP2:
				{
					bool check = _crypto.verifyCombo(_vm->va_to_pa(_vm->state()->r12), _vm->va_to_pa(_vm->state()->r10));
					char *output = NULL;
					verifyCHA(check, output);
					_vm->ram()->write(_vm->va_to_pa(_vm->state()->r12) - _vm->ram()->base(), output);
				}
				break;
			case INIT_STEP3:
				{
					_crypto.verifyNewCert(_vm->va_to_pa(_vm->state()->r12), _vm->va_to_pa(_vm->state()->r10));
				}
				break;
			case REQUEST_STEP1:
				{
					if(_crypto.verifyHPCert(_vm->va_to_pa(_vm->state()->r12), _vm->va_to_pa(_vm->state()->r10))){
						char *nonce = NULL;
						_crypto.generateNonce(Util::getNonceSize(), 1, nonce);
						_vm->ram()->write(_vm->va_to_pa(_vm->state()->r12) - _vm->ram()->base(), nonce);
					}
				}
				break;
			case REQUEST_STEP2:
				{
					int keyLen = _crypto.decryptSymmKey(_vm->va_to_pa(_vm->state()->r12), _vm->va_to_pa(_vm->state()->r10));
					char *output = NULL;
					_crypto.verifyHPSig(_vm->va_to_pa(_vm->state()->r12), _vm->va_to_pa(_vm->state()->r10), keyLen, output);
					_vm->ram()->write(_vm->va_to_pa(_vm->state()->r12) - _vm->ram()->base(), output);
				}
				break;
			case REQUEST_STEP3:
				{
					char *request = NULL;
					_crypto.authDecrypt(_vm->va_to_pa(_vm->state()->r12), _vm->va_to_pa(_vm->state()->r10), request);
					if(checkAP(request)){
						char *data = NULL;
						char *output = NULL;
						fetchData(request, data);
						_crypto.authEncrypt(data, sizeof(data), output);
						_vm->ram()->write(_vm->va_to_pa(_vm->state()->r12) - _vm->ram()->base(), output);
					} else
						_vm->ram()->write(_vm->va_to_pa(_vm->state()->r12) - _vm->ram()->base(), (char*)"Invalid request");
				}
				break;
			case REQUEST_STEP4:
				{
				char *procData = NULL;
				_crypto.authDecrypt(_vm->va_to_pa(_vm->state()->r12), _vm->va_to_pa(_vm->state()->r10), procData);
				_vm->ram()->write(_vm->va_to_pa(_vm->state()->r12) - _vm->ram()->base(), procData);
				break;
				}*/
			default:
				PERR("Unknown hypervisor call!");
				_vm->dump();
			}
		}

		/*bool checkAP(char *request){
			Genode::printf("Request is: %s\n", request);
			return true;
		}

		void fetchData(char *request, char *output){
			output = (char*)"This is placeholder data";
		}

		void verifyCHA(bool check, char *output){
			if(check){
				char *pubKey = NULL;
				char *sig = NULL;
				char *swKey = NULL;
				_crypto.genKeyPair(pubKey);
				Util::getSWKeyPath(swKey);
				_crypto.sign(pubKey, swKey, output);
				Util::join(sig, pubKey, output);
			} else {
				output = (char*)"CHA is not valid";
			}
		}*/

		bool _handle_data_abort()
		{
			PWRN("Vm tried to access %p which isn't allowed",
			     _tsc.last_failed_access());
			_vm->dump();
			return false;
		}

		bool _handle_vm()
		{
			PINF("HANDLING VM");
			switch (_vm->state()->cpu_exception) {
			case Cpu_state::DATA_ABORT:
				if (!_handle_data_abort()) {
					PERR("Could not handle data-abort will exit!");
					return false;
				}
				break;
			case Cpu_state::SUPERVISOR_CALL:
				_handle_hypervisor_call();
				break;
			default:
				PERR("Curious exception occured");
				_vm->dump();
				return false;
			}
			return true;
		}

	protected:

		void entry()
		{
			_vm->sig_handler(_sig_rcv.manage(&_vm_context));
			_vm->start();

			while (true) {
				//PINF("TO NORMAL WORLD");
				_vm->run();
				Signal s = _sig_rcv.wait_for_signal();
				//PDBG("Signal recieved");
				if (s.context() != &_vm_context) {
					PWRN("Invalid context");
					continue;
				}
				if (!_handle_vm())
					return;
			}
		};

	public:

		Vmm(addr_t tsc_base, 
			Vm    *vm)
		: Thread<8192>("vmm"),
		  _tsc_io_mem(tsc_base,     0x4000),
		  _tsc((addr_t)env()->rm_session()->attach(_tsc_io_mem.dataspace())),
		  _vm(vm)/*,
		  _crypto(_vm)*/ { 
		}
};


int main()
{
	PINF("Main entered");
	enum {
		TSC_IMX6_BASE     = 0x021D0000, // Length = 0x4000
		MAIN_MEM_START    = Trustzone::NONSECURE_RAM_BASE,
		MAIN_MEM_SIZE     = Trustzone::NONSECURE_RAM_SIZE,
		KERNEL_OFFSET     = 0x8000,
		MACH_TYPE         = 4296, // voor nitrogen6x
				/*3769, voor SABRElite source: www.arm.linux.org.uk/developer/machines*/
	};
	PINF("enum made");

	static const char* cmdline = "console=ttymxc1,115200 init=/init video=mxcfb0:dev=hdmi,1920x1080M@60,if=RGB24 fbmem=10M vmalloc=400M androidboot.console=ttymxc1 loglevel=10 earlyprintk";

	PINF("cmdline made");

	static Vm vm("android", /*"initrd.gz",*/ cmdline, MAIN_MEM_START, MAIN_MEM_SIZE,
	             KERNEL_OFFSET, MACH_TYPE);

	PINF("VM made");

	static Vmm::Vmm vmm(TSC_IMX6_BASE, 
	                    &vm);

	PINF("Start virtual machine ...");
	vmm.start();
	//vmm.entry();

	sleep_forever();
	return 0;
}
