/*
 * \brief  Board definitions for the Freescale i.MX6
 * \author Nikolay Golikov <nik@ksyslabs.org>
 * \author Josef Soentgen
 * \author Martin Stein
 * \date   2014-02-25
 */

/*
 * Copyright (C) 2014-2015 Ksys Labs LLC
 * Copyright (C) 2014-2015 Genode Labs GmbH
 *
 * This file is part of the Genode OS framework, which is distributed
 * under the terms of the GNU General Public License version 2.
 */

#ifndef _DRIVERS__BOARD_BASE_H_
#define _DRIVERS__BOARD_BASE_H_

namespace Genode
{
	/**
	 * i.MX6 motherboard
	 */
	struct Board_base;
}

struct Genode::Board_base
{
	enum {
		/* normal RAM */
		RAM0_BASE = 0x40000000,
		RAM0_SIZE = 0x10000000,
		RAM1_BASE = 0x10000000,
		RAM1_SIZE = 0x30000000,

		/* device IO memory */
		MMIO_BASE = 0x00000000,
		MMIO_SIZE = 0x10000000,

		UART_1_IRQ       = 58,
		UART_1_MMIO_BASE = 0x02020000,
		UART_1_MMIO_SIZE = 0x00004000,

		UART_2_IRQ       = 59,
		UART_2_MMIO_BASE = 0x021e8000,
		UART_2_MMIO_SIZE = 0x00004000,

		/* timer */
		EPIT_1_IRQ       = 88,
		EPIT_1_MMIO_BASE = 0x020d0000,
		EPIT_1_MMIO_SIZE = 0x00004000,

		EPIT_2_IRQ       = 89,
		EPIT_2_MMIO_BASE = 0x020d4000,
		EPIT_2_MMIO_SIZE = 0x00004000,

		/* ARM IP Bus control */
		AIPS_1_MMIO_BASE = 0x0207c000,
		AIPS_1_MMIO_SIZE = 0x00004000,
		AIPS_2_MMIO_BASE = 0x0217c000,
		AIPS_2_MMIO_SIZE = 0x00004000,
			
		GPIO1_MMIO_BASE    = 0x0209C000,
		GPIO1_MMIO_SIZE    = 0x00004000,
		GPIO2_MMIO_BASE    = 0x020A0000,
		GPIO2_MMIO_SIZE    = 0x00004000,
		GPIO3_MMIO_BASE    = 0x020A4000,
		GPIO3_MMIO_SIZE    = 0x00004000,
		GPIO4_MMIO_BASE    = 0x020A8000,
		GPIO4_MMIO_SIZE    = 0x00004000,
		GPIO5_MMIO_BASE    = 0x020AC000,
		GPIO5_MMIO_SIZE    = 0x00004000,
		GPIO6_MMIO_BASE    = 0x020B0000,
		GPIO6_MMIO_SIZE    = 0x00004000,
		GPIO7_MMIO_BASE    = 0x020B4000,
		GPIO7_MMIO_SIZE    = 0x00004000,
		GPIO1_IRQL         = 98,
		GPIO1_IRQH         = 99,
		GPIO2_IRQL         = 100,
		GPIO2_IRQH         = 101,
		GPIO3_IRQL         = 102,
		GPIO3_IRQH         = 103,
		GPIO4_IRQL         = 104,
		GPIO4_IRQH         = 105,
		GPIO5_IRQL         = 106,
		GPIO5_IRQH         = 107,
		GPIO6_IRQL         = 108,
		GPIO6_IRQH         = 109,
		GPIO7_IRQL         = 110,
		GPIO7_IRQH         = 111,

		I2C_1_MMIO_BASE     = 0x021A0000,
		I2C_1_MMIO_SIZE     = 0x00004000,
		I2C_1_IRQ           = 68,

		I2C_2_MMIO_BASE     = 0x021A4000,
		I2C_2_MMIO_SIZE     = 0x00004000,
		I2C_2_IRQ           = 69,

		I2C_3_MMIO_BASE     = 0x021A8000,
		I2C_3_MMIO_SIZE     = 0x00004000,
		I2C_3_IRQ           = 70,

		CSU_BASE            = 0x021C0000,
		CSU_SIZE            = 0x00004000,
		CSU_IRQ             = 53,

		CCM_BASE	    = 0x020C4000,
		CCM_SIZE	    = 0x00004000,
		CCM_IRQ1	    = 119,
		CCM_IRQ2	    = 120,

		/* L2 cache */
		PL310_MMIO_BASE = 0x00a02000,
		PL310_MMIO_SIZE = 0x00001000,

		//PLL1_CLOCK = 800*1000*1000,

		/* CPU */
		CORTEX_A9_PRIVATE_MEM_BASE  = 0x00a00000,
		CORTEX_A9_PRIVATE_MEM_SIZE  = 0x00002000,
		//CORTEX_A9_PRIVATE_TIMER_CLK = PLL1_CLOCK,
		CORTEX_A9_PRIVATE_TIMER_CLK = 395037500,

		/* CPU cache */
		CACHE_LINE_SIZE_LOG2 = 5,

		/* wether board provides ARM security extension */
		SECURITY_EXTENSION = 1,
	};
};

#endif /* _DRIVERS__BOARD_BASE_H_ */
